Required Modules
================
[shapely](https://pypi.python.org/pypi/Shapely)

[pyshp](https://pypi.python.org/pypi/pyshp)

[numpy](https://pypi.python.org/pypi/numpy)


Example
=======

```python
import fractal_dim as frac
ruler = frac.RulerMethod()
ruler.load('my_shape.shp')
r_count, shape_1 = ruler.count(1)  # use segments of length 1 unit

box = frac.BoxMethod()
box.load('my_shape.shp')
b_count, boxes_1 = box.count(1)  # use boxes with side of length 1 unit

# plotting the shapes for comparisons
import matplotlib.pyplot as plt
rx, ry = ruler.shape.xy
x, y = shape_1.xy
for b in boxes_1:
    bx, by = b.exterior.xy
    plt.plot(bx, by, 'k-')
plt.plot(x, y, 'b-', rx, ry, 'r-')
plt.show()
```

How to cite
===========
[Rezvanbehbahani, S., van der Veen, C.J. & Stearns, L.A. Math Geosci (2019). https://doi.org/10.1007/s11004-019-09788-7](https://link.springer.com/article/10.1007/s11004-019-09788-7)