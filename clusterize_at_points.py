"""Run fractal_at_points for a folder of shapefiles."""
import os
import subprocess
import argparse
import fractal_dim as frac

python = '/cresis/snfs1/scratch/lbyers/sw/anaconda2/envs/fractals/bin/python'
at_points_script = '/cresis/snfs1/scratch/lbyers/fractals/src/160321_at_points.py'
def qsub_header(name, mem='3gb'):
    header = '#!/bin/sh\n' + \
             '#PBS -l nodes=1:ppn=1\n#PBS -l mem=' + mem + '\n' \
             '#PBS -k o\n#PBS -j oe\n#PBS -V\n' + \
             '#PBS -N ' + name + '\n\n'
    return header

def qscript_at_points(fname, qname, script, shapefile, radius, num, outfile, plot):
    txt = qsub_header(qname, mem='3gb')
    txt = txt + ' '.join([python, script, shapefile, str(radius), str(num), outfile, '--plot', plot])
    with open(fname, 'w') as f:
        f.write(txt)
    return fname


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Clusterize fractal at points')
    parser.add_argument('--script', type=str, default=at_points_script)
    parser.add_argument('shpdir', type=str)
    parser.add_argument('radius', type=float)
    parser.add_argument('num', type=int)
    parser.add_argument('outdir', type=str)
    parser.add_argument('plotdir', type=str)
    parser.add_argument('--qscriptdir', type=str, default='.')
    parser.add_argument('--dynamicradius', action='store_true')
    args = parser.parse_args()

    shapefiles = []
    outfiles = []
    plotfiles = []
    for f in os.listdir(args.shpdir):
        base = os.path.splitext(f)[0]
        ext = os.path.splitext(f)[-1]
        if ext == '.shp':
            shapefiles.append(os.path.join(args.shpdir, f))
            outfiles.append(os.path.join(args.outdir, base + '.csv'))
            plotfiles.append(os.path.join(args.plotdir, base + '.png'))
            print f

    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
    if not os.path.exists(args.plotdir):
        os.mkdir(args.plotdir)
    if not os.path.exists(args.qscriptdir):
        os.mkdir(args.qscriptdir)
    for i, (shp, fout, plout) in enumerate(zip(shapefiles, outfiles, plotfiles)):
        qname = 'q_frac__' + str(i)
        qscript = os.path.join(args.qscriptdir, qname + '.script')
        if args.dynamicradius:
            r = frac.RulerMethod()
            r.load(shp)
            radius = r.shape.length / args.num
        else:
            radius = args.radius
        qscript_at_points(qscript, qname, args.script, shp, \
                          radius, args.num, fout, plout)
        subprocess.call(['qsub', qscript])
        print i, len(shapefiles)
