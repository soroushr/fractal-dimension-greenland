"""Get fractal dimension in a neighborhood of points."""
import argparse
import os
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import fractal_dim as frac

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get D for local regions')
    parser.add_argument('shapefile', type=str)
    parser.add_argument('radius', type=float)
    parser.add_argument('num', type=int)
    parser.add_argument('outfile', type=str)
    parser.add_argument('--plot', type=str)
    args = parser.parse_args()

    ruler = frac.RulerMethod()
    ruler.load(args.shapefile)
    test_shape = ruler.shape
    pts, dims = frac.fractal_at_points(test_shape, args.radius, args.num)
    xy = frac.array(test_shape.coords[:])
    ptxy = frac.array([p.coords[0] for p in pts])
    np.savetxt(args.outfile, zip(ptxy[:, 0], ptxy[:, 1], dims), delimiter=',')

    if args.plot:
        plt.plot(xy[:, 0], xy[:, 1], 'k-')
        ax = plt.gca()
        ax.set_aspect('equal')
        plt.scatter(ptxy[:, 0], ptxy[:, 1], c=dims, s=50, vmin=1, vmax=1.25)
        plt.colorbar()
        plt.suptitle(args.shapefile)
        plt.savefig(args.plot, dpi=100)
