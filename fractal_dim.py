#! usr/bin/env python
#
#  this is free software
#
#  Copyright 2015 Logan C Byers
#
#  Authors: Logan C Byers
#  Contact: loganbyers@ku.edu
#  Date: 2015.08.23
#  Modified: 2015.10.07
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#
###############################################################################

from exceptions import NotImplementedError

from shapely.geometry import Point, LineString, Polygon, MultiPolygon
from shapely.geometry.base import BaseGeometry
from shapefile import Reader as ShapefileReader
from numpy import array, power, ceil, sqrt, logspace, log10, zeros_like
from scipy.optimize import curve_fit

def fractal_dimension(segment_lengths, counts):
    """
    Compute the fractal dimension

    Parameters
    ----------
    segment_lengths: array-like
        List of lengths of segments
    counts: array-like
        List of segment counts

    Returns
    -------
    D: float
        fractal dimension
    """
    powerlaw = lambda x, a, b: a * power(x, b)
    perimeters = array(counts) * array(segment_lengths)
    popt, pcov = curve_fit(powerlaw, segment_lengths, perimeters)
    return 1 - popt[1]




def Koch_snowflake(levels, edge=1):
    """
    Construct a Koch snowflake to a given level of detail.

    Parameters
    ----------
    levels: int
        Iterations used to approximate the snowflake; 0 for a triangle.
    edge: float, optional
        Edge length for a 0-level snowflake.

    Returns
    -------
    shape: LineString
        Koch snowflake.

    """
    rad_3_2 = sqrt(3)/2
    shape = LineString([[0, 0], [0.5 * edge, rad_3_2 * edge],
                        [-0.5 * edge, rad_3_2 * edge], [0, 0]])
    for level in range(levels):
        points = [shape.coords[0]]
        for i in range(len(shape.coords) - 1):
            x0, y0 = shape.coords[i]
            x1, y1 = shape.coords[i + 1]
            points.extend([[x0 + (x1 - x0) / 3, y0 + (y1 - y0) / 3],
                           [x0 + (x1 - x0) / 2 + rad_3_2 / 3 * (y1 - y0),
                            y0 + (y1 - y0) / 2 + rad_3_2 / 3 * (x0 - x1)],
                           [x0 + 2 * (x1 - x0) / 3, y0 + 2 * (y1 - y0) / 3],
                           [x1, y1]])
        shape = LineString(points)
    return shape

class BaseFractalDimension(object):

    """Base class for fractal dimension methods."""

    def __init__(self):
        """Pass."""
        self.name = None
        self.points = None
        self.shape = None
        pass

    def load(self, shape, *args, **kwargs):
        """
        Load a shape, which could be a shapefile or a shapely geometry.

        Parameters
        ----------
        shape: str or shape representation
            shape to load for analysis
        *args: listed arguments
        **kwargs: keyworded arguments

        """
        # load shape from file
        if type(shape) is str:
            if shape[-4:].lower() == '.shp':
                self._load_shapefile(shape)
                return
            else:
                # maybe allow WKT files
                raise NotImplementedError()
        # load shape from shapely object
        elif isinstance(shape, BaseGeometry):
            self.shape = shape
            return
        # other options, to be implemented later perhaps
        else:
            raise NotImplementedError()

    def count(self, length, *args, **kwargs):
        """
        Get the number of units to describe the shape.

        Parameters
        ----------
        length : float
            metric to describe the unit of measurement

        Returns
        -------
        count: int
            number of units
        shape: GeometryCollection
            shapely collection of the units

        """
        raise NotImplementedError()

    def fractal_dimension(self, lengths, *args, **kwargs):
        """
        Get the fractal dimension of the shape.

        The fractal dimension is computed as the linear regression of the
        log-log relation between unit length and count. The set of lengths
        to consider is given by the `lengths` argument.

        Parameters
        ----------
        lengths: list-like of floats
            set of lengths to use

        Returns
        -------
        dimension: float
            fractal dimension of the shape

        """
        raise NotImplementedError()

    def plot(self, fname=None, **kwargs):
        """
        Get a graphical representation of dimensionality.

        This function generates a plot of the relation between the unit length
        and the unit count. The plot will be saved to file if `fname` is given
        in the call. Specifications for how to save the plot are passed with
        `**kwargs`.

        Parameters
        ----------
        fname: str, optional
            filename for saved plot file

        """
        raise NotImplementedError()

    def _load_shapefile(self, shp):
        """
        Load the first shape from a shapefile.

        Parameters
        ----------
        shp: str
            shapefile to read

        Returns
        -------
        shape: BaseGeometry
            first feature from the shapefile

        """
        self.name = shp
        self.points = ShapefileReader(shp).shapes()[0].points
        self.shape = LineString(self.points)
        # if closed (last point == first point), remove last point
        if self.shape.is_closed:
            x, y = self.shape.xy
            self.shape = LineString(zip(x[:-1], y[:-1]))


class BoxMethod(BaseFractalDimension):

    """A class for using the box method algorithm."""

    def __init__(self):
        """Pass."""
        super(BoxMethod, self).__init__()
        pass

    def count(self, length, *args, **kwargs):
        """
        Get the number of boxes in a grid that intersect the object.

        Currently, the grid is aligned so that the centroid of the shape
        falls on the boundaries between boxes.

        Parameters
        ----------
        length: float
            Length of side of the square boxes.

        Returns
        -------
        count: int
            Number of boxes that intersect the shape
        shape: MultiPolygon
            Set of boxes.
        """
        ox, oy = self.shape.envelope.centroid.coords[0]
        nx_left = int(ceil((ox - self.shape.bounds[0])/length))
        nx_right = int(ceil((self.shape.bounds[2] - ox)/length))
        ny_down = int(ceil((oy - self.shape.bounds[1])/length))
        ny_up = int(ceil((self.shape.bounds[3] - oy)/length))
        boxes = []
        for i in range(-nx_left, nx_right + 1):
            for j in range(-ny_down, ny_up + 1):
                box = Polygon([(ox + i * length, oy + j * length),
                               (ox + (i + 1) * length, oy + j * length),
                               (ox + (i + 1) * length, oy + (j + 1) * length),
                               (ox + i * length, oy + (j + 1) * length)])
                if box.intersects(self.shape):
                    boxes.append(box)
        boxes = MultiPolygon(boxes)
        return len(boxes), boxes


class RulerMethod(BaseFractalDimension):

    """A class for using the ruler method algorithm."""

    def __init__(self):
        """Pass."""
        super(RulerMethod, self).__init__()
        pass

    def count(self, length, start=None):
        """
        Get the number of segments of a certain length to describe the object.

        Parameters
        ----------
        length: float
            Line segment length
        start: float, optional
            Distance along the object to use as the start point

        Returns
        -------
        count: int
            Number of line segments to describe the object.
        line_shape: LineString
            The resulting shape from the ruler method.

        """
        if start is None:
            start = 0.0
        distance = start  # distance of the current point along the shape
        count_num = 0     # number of segments utilized so far
        next_points = []  # points to be used for making the result shape
        while distance < self.shape.length:
            # get the current point from the distance
            point = self.shape.interpolate(distance)
            # construct a circle around the current point
            circle = point.buffer(length, 512)  # (radius, circle_divisions)
            # get the intersection of the shape and the created circle
            segments = circle.intersection(self.shape)
            # deal with various potential segment types from the intersection
            if segments.type == 'MultiLineString':
                next_point = self._next_point_multi(point, segments, distance)
            elif segments.type == 'LineString':
                next_point = self._next_point(point, segments, distance)
            elif segments.type == 'GeometryCollection':
                next_point = self._next_point_geometry(point, segments, distance)
            else:
                print 'Error in RulerMethod.count <' + str(segments.type) + '>, returning -1'
                return -1
            if next_point is None:
                    return count_num + 1, LineString(next_points)
            # compute new distance along shape from the new point
            distance = self.shape.project(next_point)
            # add the new point to the list for the result shape
            next_points.append(next_point)
            # increase the count by 1
            count_num += 1
        if len(next_points) < 2:
            return count_num, None
        return count_num, LineString(next_points)

    def _next_point(self, point, segment, distance):
        """
        Get the next point from a single LineStrings.

        Parameters
        ----------
        point: Point
            Current position of circle center.
        line_segments: LineString
            Line segment within the circle.
        distance: float
            Current distance along the master LineString

        Returns
        -------
        next_point: Point or None
            Next end point of segment along master LineString.
            None if `segment` is entirely behind or ahead of `point`,
            otherwise the next point.

        """
        # get starting and ending points of the linestring
        x, y = segment.xy
        start = Point(x[0], y[0])
        end = Point(x[-1], y[-1])
        # get distances along the original shape for the start and end points
        start_dist = self.shape.project(start)
        end_dist = self.shape.project(end)
        # if linestring is practically a point
        if start_dist == end_dist:
            return None
        # if linestring is entirely behind the current position
        if start_dist < distance and end_dist < distance:
            return None
        # if current position is within linestring
        elif start_dist <= distance and end_dist > distance:
            return self.shape.interpolate(end_dist)
        # if current position is within linestring
        elif end_dist <= distance and start_dist > distance:
            return self.shape.interpolate(start_dist)
        # if linestring is entirely in front of current position
        else:
            return self.shape.interpolate(start_dist)


    def _next_point_multi(self, point, line_segments, distance):
        """
        Get the next point from many disconnected LineStrings.

        Parameters
        ----------
        point: Point
            Current position of circle center.
        line_segments: MultiLineString
            Line segments within the circle.
        distance: float
            Current distance along the master LineString

        Returns
        -------
        next_point: Point
            Next end point of LineString along master LineString

        """
        for segment in line_segments:
            p = self._next_point(point, segment, distance)
            if p is not None:
                return p

    def _next_point_geometry(self, point, collection, distance):
        """
        Get the next point from many disconnected geometry types.

        Parameters
        ----------
        point: Point
            Current position of circle center.
        collection: GeometryCollection
            Objects within the circle.
        distance: float
            Current distance along the master LineString

        Returns
        -------
        next_point: Point
            Next end point of LineString along master LineString

        """
        for geom in collection:
            if geom.type == 'LineString':
                p = self._next_point(point, geom, distance)
            elif geom.type == 'MultiLineString':
                p = self._next_point_multi(point, geom, distance)
            elif geom.type == 'Point':
                continue
            else:
                print 'Error in RulerMethod.next_point_geometry <' + str(geom.type) + '>, returning -1'
                return -1
            if p is not None:
                return p



def fractal_at_points(shape, cutradius, num):
    """Get fractal dimension local to a set of points."""
    along_line_dist = shape.length/float(num)
    points = [shape.interpolate(d * along_line_dist) for d in range(num)]
    fractal_dims = []
    ruler = RulerMethod()
    ruler.shape = shape
    for i_pt, pt in enumerate(points):
        local_circle = pt.buffer(cutradius, 500)
        local_xsect = local_circle.intersection(shape)
        if local_xsect.type == 'LineString':
            local_line = local_xsect
            pass
        elif local_xsect.type == 'MultiLineString':
            for seg in local_xsect:
                nxt_pt = ruler._next_point(pt, seg, i_pt * along_line_dist)
                if nxt_pt is not None:
                    break
            local_line = seg
        elif local_xsect.type == 'GeometryCollection':
            for seg in local_xsect:
                if seg.type == 'Point':
                    continue
                elif seg.type == 'LineString':
                    nxt_pt = ruler._next_point(pt, seg, i_pt * along_line_dist)
                    if nxt_pt is not None:
                        local_line = seg
                        break
                elif seg.type == 'MultiLineString':
                    for s in seg:
                        nxt_pt = ruler._next_point(pt, s, i_pt * along_line_dist)
                        if nxt_pt is not None:
                            local_line = s
                            break
                else:
                    print 'Error in RulerMethod.next_point_geometry <' + str(geom.type) + '>, returning -1'
                    return -1

        local_ruler = RulerMethod()
        local_ruler.shape = local_line
        local_perim = local_line.length
        segments = logspace(log10(local_perim/1000.0), log10(local_perim/4.0), 10)
        counts = zeros_like(segments)

        for i, seg in enumerate(segments):
            counts[i], _ = local_ruler.count(seg)
        print i_pt
        fractal_dims.append(fractal_dimension(segments, counts))
    return points, fractal_dims
